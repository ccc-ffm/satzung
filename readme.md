# Satzung des Chaos Computer Club Frankfurt e.V.

Diese [Satzung](satzung.typ) wurde mit [Typst](https://typst.app) formatiert.

## Build

Zum Bauen des Dokumentes:

```shell
docker run --rm -u $(id -u):$(id -g)  -w /data -v "$PWD":/data ghcr.io/typst/typst:latest compile satzung.typ
```

Um die Satzung bei Änderungen neu zu bauen, kann typst mit `watch satzung.typ` aufgerufen werden
