// The project function defines how your document looks.
// It takes your content and some metadata and formats it.
// Go ahead and customize it to your liking!
#let project(title: "", version: "", datum: "", body) = {
  // Set the document's basic properties.
  set document(author: "Chaos Computer Club Frankfurt e.V.", title: title)
  set page(
    numbering: "1",
    number-align: center,
    footer: [
      #grid(
        columns: (75%, 25%),
        rows: auto,
        [
          #set text(1em)
          #set align(left)
          Version $#version$ vom $#datum$
        ],
        [
          #set align(right)
          #context {
              counter(page).display(
                "1 von 1",
                both: true,
              )
          }
        ],
      )
    ]
  )
  set text(font: "Libertinus Serif", lang: "de")
  set heading(numbering: "1.1")

  // Title row.
  align(center)[
    #block(text(weight: 700, 1.75em, title))
  ]

  v(1.5em, weak: true)

  align(center)[
    #block(text(
      weight: 700,
      1.25em, 
      "Version "+version+" vom "+datum))
  ]

  v(3em, weak: true)
  // Main body.
  set par(justify: true)
  body
}
