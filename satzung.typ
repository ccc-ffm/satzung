#import "template.typ": *
#show: project.with(
  title: "Satzung des Chaos Computer Club Frankfurt e.V.",
  version: "1.8",
  datum: "07.12.2024"
)

#set heading(numbering: "§ 1")

= Name, Sitz, Geschäftsjahr, Grundsätzliches
#set enum(numbering: n => "1." + [#n])
+ Der Verein führt den Namen „Chaos Computer Club Frankfurt“. Der Verein wird in das Vereinsregister eingetragen und dann um den Zusatz „e.V.“ ergänzt.
+ Der Verein hat seinen Sitz in Frankfurt am Main.
+ Das Geschäftsjahr ist das Kalenderjahr.

= Zweck
#set enum(numbering: n => "2." + [#n])
+ Der Zweck des Vereins ist die Förderung
  - der Bildung (§ 52 Abs. 2 Nr. 7 AO)
  - des Verbraucherschutzes (§ 52 Abs. 2 Nr. 16 AO)
+ Der Satzungszweck wird verwirklicht insbesondere durch
  - Veranstaltungen in unterschiedlichen Formaten zum Meinungs- und Wissensaustausch bzw. die Wissensvermittlung insbesondere über Informations- und Kommunikationsmedien, über die zugrundeliegende Technik allgemein sowie die Beratung und Aufklärung der Anwendenden solcher Technologien;
  - das regelmäßige Abhalten öffentlicher Treffen und Informationsveranstaltungen an denen alle teilnehmen können, um einen regelmäßigen Austausch über beispielsweise Informationssysteme anzuregen und zum gemeinsamen Lösen von bestehenden Problemen an Informationssystemen und technologischen Geräten;
  - die Organisation und/oder Förderung von Seminaren, Kongressen, Konferenzen und virtuellen Zusammenkünften insbesondere mit Inhalten der Computersicherheit, des Datenschutzes, der Sicherheit und des kreativen Umgangs mit neuen Technologien und deren Anwendungen, sowie des Upcyclings von Geräten durch die Anpassung von Hard- und Software;
  - Förderung des schöpferisch-kritischen Umgangs mit Technologie und die langfristige, nachhaltige Technologiefolgenabschätzung, insbesondere durch die Schaffung und den Erhalt eines Makerspaces/RepairCafes, Veranstaltung von Workshops zur Vermittlung von technischem Wissen rund um technologische Geräte (Vermittlung von Kenntnissen zum Löten, Lasercutting, 3D-Druck, elektrische Grundkenntnisse, etc.) sowie des Upcyclings von solchen Geräten durch die Anpassung von Hard- und Software;
  - Information und Austausch über die Möglichkeiten der Nutzung von Freier Software (Heimautomatisierung, Verkehrssysteme, Freifunk, Betriebssysteme, etc.);
  - Angebote, die den Zugang zu technischen Geräten und Informationssystemen für marginalisierte Gruppen erleichtern;
  - Arbeits- und Erfahrungsaustauschkreise.
+ Die Satzungszwecke werden auch durch Zusammenwirken mit weiteren gemeinnützigen Körperschaften verwirklicht. Der Verein kann insoweit auch mit diesen Kooperationen für einzelne Veranstaltungen oder allgemeiner Art eingehen und hierzu einen entsprechenden Kooperationsvertrag abschließen.
+ Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke" der Abgabenordnung (AO). Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke. Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins. Es darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.
+ Für den Verein tätige Personen erhalten eine Erstattung der nachgewiesenen angemessenen Aufwendungen; das Nähere kann in einer Geschäftsordnung durch die Mitgliederversammlung geregelt werden.
+ Im Rahmen der haushaltsrechtlichen Möglichkeiten erfolgt die Gewährung angemessener Vergütungen für Dienstleistungen bis zur Höhe der Ehrenamtspauschale gem. § 3 Nr. 26a EStG durch Vorstandsbeschluss, darüber hinaus nur aufgrund einer schriftlichen Vereinbarung.
+ Angemessene Vergütungen für Vorstandsmitglieder sind unabhängig davon, ob sie für die Vorstandstätigkeit als solche oder andere Dienstleistungen erfolgen, von der Mitgliederversammlung zu genehmigen. Ein mit dem Vorstand als Vorstand geschlossener Dienstvertrag endet, ohne dass es einer gesonderten Kündigung bedarf, mit dem Ende der Amtszeit des Vorstandsmitgliedes. Umgekehrt endet die Amtszeit mit Beendigung des Anstellungsverhältnisses mit dem Vorstand.


= Mitgliedschaft
#set enum(numbering: n => "3." + [#n])
+ Dem Verein gehören an:
  - Ordentliche Mitglieder
  - Fördermitglieder
  - Ehrenmitglieder
+ Ordentliche Mitglieder können nur natürliche Personen werden. Bei Minderjährigen ist die Zustimmung des gesetzlichen Vertreters erforderlich.
+ Fördermitglieder können natürliche Personen sein. Bei Minderjährigen ist die Zustimmung des gesetzlichen Vertreters erforderlich. Fördermitglieder fördern die Vereinsziele vorwiegend durch einen Mitgliedsbeitrag. Fördermitglieder haben kein Stimmrecht, dürfen jedoch an der Mitgliederversammlung beratend teilnehmen.
+ Die Mitgliederversammlung kann solche Personen, die sich besondere Verdienste um den Verein oder um die von ihm verfolgten satzungsgemässen Zwecke erworben haben, zu Ehrenmitgliedern ernennen.
+ Die Beitrittserklärung erfolgt schriftlich oder fernschriftlich gegenüber dem Vorstand. Über die Annahme der Beitrittserklärung entscheidet der Vorstand. Die Mitgliedschaft beginnt mit der Annahme der Beitrittserklärung durch den Vorstand. Eine Ablehnung des Aufnahmeantrags ist nicht anfechtbar und muss nicht begründet werden.
+ Die Mitgliedschaft endet durch Austrittserklärung, durch Ausschluss oder durch Tod von natürlichen Personen.
+ Der Austritt erfolgt durch schriftliche Erklärung gegenüber dem Vorstand. Die Frist für die Austrittserklärung beträgt 3 Monate.
+ Ein Mitglied kann jederzeit mit sofortiger Wirkung durch einfachen Mehrheitsbeschluss des Vorstands ausgeschlossen werden, wenn es in grober Weise gegen die Interessen des Vereins verstößt, das Ansehen des Vereins schädigt, seinen Beitragsverpflichtungen nicht nachkommt, oder ein sonstiger wichtiger Grund vorliegt. Der Vorstand muss dem auszuschließenden Mitglied den Beschluss in schriftlicher Form unter Angabe von Gründen mitteilen und ihm auf Verlangen eine Anhörung gewähren. Gegen den Beschluss des Vorstandes ist die Anrufung der Mitgliederversammlung zulässig. Bis zum Beschluss der Mitgliederversammlung ruht die Mitgliedschaft.

= Rechte und Pflichten der Mitglieder
#set enum(numbering: n => "4." + [#n])
+ Die Mitglieder sind verpflichtet, die satzungsgemäßen Zwecke des Vereins zu unterstützen und zu fördern.
+ Die Mitglieder sind verpflichtet, die festgesetzten Mitgliedsbeiträge zu zahlen.
+ Die Mitglieder sind verpflichtet ihre erreichbare E-Mail-Adresse sowie ihre postalische Anschrift anzugeben und bei Änderung diese dem Vereinsvorstand gegenüber mitzuteilen.

= Mitgliedsbeiträge
#set enum(numbering: n => "5." + [#n])
+ Die vom Verein zu erhebenden Aufnahme- bzw. Mitgliedsbeiträge werden in einer gesonderten Beitragsordnung festgesetzt, die von der Mitgliederversammlung beschlossen wird.
+ Ehrenmitglieder sind von Aufnahme- bzw. Mitgliedsbeiträgen befreit.
+ Im Falle nicht fristgerechter Entrichtung der Beiträge ruht die Mitgliedschaft.
+ An die Stelle der Mitgliedsbeiträge können mit Genehmigung des Vorstands andere gleichwertige Zuwendungen treten.
+ Im begründeten Einzelfall kann für ein Mitglied durch Vorstandsbeschluss ein von der Beitragsordnung abweichender Beitrag festgesetzt werden.

= Organe des Vereins
#set enum(numbering: n => "6." + [#n])
+ Die Organe des Vereins sind:
  - die Mitgliederversammlung
  - der Vorstand

= Mitgliederversammlung
#set enum(numbering: n => "7." + [#n])
+ Die Mitgliederversammlung ist zuständig für:
  - die Wahl und Abberufung der Vorstandsmitglieder
  - die Wahl der Finanzprüfenden
  - die Genehmigung des vom Vorstand aufgestellten Haushaltsplans für das nächste Geschäftsjahr
  - die Entgegennahme des Jahresberichts und die Entlastung des Vorstands
  - die Festsetzung der Höhe und der Fälligkeit des Aufnahme- sowie Mitgliedsbeitrags und die Genehmigung der Beitragsordnung
  - den Beschluss einer Richtlinie über die Erstattung von Reisekosten und Auslagen des Vorstands
  - die Beschlussfassung über Satzungsänderungen
  - Anträge des Vorstands und der Mitglieder
  - die Auflösung des Vereins
+ Zur Teilnahme an der Mitgliederversammlung sind sämtliche Mitglieder berechtigt.
+ Die ordentliche Mitgliederversammlung wird mindestens einmal in zwei Jahren abgehalten.
+ Außerordentliche Mitgliederversammlungen werden auf Beschluss des Vorstandes abgehalten, wenn die Interessen des Vereins dies erfordern oder wenn mindestens 30% der ordentlichen Mitglieder dies unter Angabe des Zwecks schriftlich beantragen.
+ Die Einberufung der Mitgliederversammlung erfolgt in Textform durch den Vorstand mit einer Frist von mindestens zwei Wochen. Zur Wahrung der Frist reicht die Aufgabe der Einladung zur Post an die letzte bekannte Anschrift oder die Versendung an die zuletzt bekannte E-Mail-Adresse. Der Einladung ist die Tagesordnung beizufügen.
+ Die Tagesordnung wird vom Vorstand festgelegt. Anträge zur Mitgliederversammlung müssen bis spätestens 8 Tage vorher schriftlich beim Vorstand eingegangen sein.
+ Jedes ordentliche Mitglied hat eine Stimme.
+ Ehrenmitglieder sowie Fördermitglieder sind nicht stimmberechtigt, können jedoch beratend an Mitgliederversammlungen teilnehmen.
+ Die Mitgliederversammlung ist beschlussfähig, wenn mindestens 15 Prozent aller stimmberechtigten Mitglieder anwesend sind. Beschlüsse sind jedoch gültig, wenn die Beschlussfähigkeit vor der Beschlussfassung nicht angezweifelt wurde. Ist die Mitgliederversammlung aufgrund mangelnder Teilnehmerzahl nicht beschlussfähig, ist die darauf folgende ordentlich einberufene Mitgliederversammlung ungeachtet der Teilnehmerzahl beschlussfähig.
+ Beschlussfassung der Mitgliederversammlung erfolgt durch Handzeichen mit einfacher Mehrheit. Auf Antrag wird geheim abgestimmt. Satzungsänderungen bedürfen einer Mehrheit von 3/4 der abgegebenen Stimmen. Die Auflösung des Vereins kann nur mit einer Mehrheit von 4/5 der abgegebenen Stimmen beschlossen werden. Zur Änderung des Zwecks des Vereins bedarf es einer Mehrheit von 3/4 der abgegebenen Stimmen.
+ Beschlüsse der Mitgliederversammlung werden innerhalb von zwei Wochen nach der Mitgliederversammlung durch die schriftführende Person oder eine vertretende Person in einem Protokoll niedergelegt und von einem weiteren Vorstandsmitglied unterzeichnet. Es soll folgende Feststellungen enthalten: Ort und Zeit der Versammlung, die Zahl der erschienenen Mitglieder, die Tagesordnung, die einzelnen Abstimmungsergebnisse und die Art der Abstimmung. Bei Satzungsänderungen soll der genaue Wortlaut angegeben werden. Eine Abschrift des Protokolls ist jedem Mitglied zugänglich zu machen.

= Der Vorstand
#set enum(numbering: n => "8." + [#n])
+ Der Vorstand besteht aus fünf gleichberechtigten Mitgliedern von denen einer explizit als kassenführende Person und einer als schriftführende Person von der Mitgliederversammlung gewählt wird.
+ Die Mitgliederversammlung kann zwei weitere stimmberechtigte Vorstandsmitglieder als nicht vertretungsberechtigte beisitzende Personen bestellen.
+ Der Vorstand wird von der Mitgliederversammlung auf die Dauer von zwei Jahren gewählt. Er bleibt jedoch solange im Amt, bis ein neuer Vorstand ordnungsgemäß gewählt ist.
+ Scheidet ein Mitglied des Vorstandes vorzeitig aus, so wählt die Mitgliederversammlung für die restliche Amtsdauer des ausgeschiedenen Vorstandsmitglieds eine nachfolgende Person.
+ In den Vorstand dürfen nur natürliche Personen gewählt werden. Die Wahl erfolgt mit einfacher Mehrheit. Wiederwahl ist zulässig.
+ Die Mitglieder des Vorstands müssen ordentliche Mitglieder des Vereins sein.
+ Sind mehr als zwei Vorstandsmitglieder dauernd an der Ausübung ihres Amtes gehindert, so sind unverzüglich Nachwahlen anzuberaumen.
+ Jedes Vorstandsmitglied (mit Ausnahme der beisitzenden Personen) ist allein vertretungsberechtigt nach §26 BGB. Ausgenommen sind Einstellung und Entlassung von Angestellten, die Schaltung von Anzeigen, die Aufnahme von Krediten sowie Rechtsgeschäfte zu Lasten des Vereins ab einer Höhe von 500€, bei welchen die vorherige Zustimmung des Gesamtvorstands einzuholen ist.
+ Die Vorstandsmitglieder sind grundsätzlich ehrenamtlich tätig; sie haben gegebenenfalls Anspruch auf Erstattung notwendiger Auslagen im Rahmen einer von der Mitgliederversammlung zu beschliessenden Richtlinie über die Erstattung von Reisekosten und Auslagen.
+ Der Vorstand führt die laufenden Geschäfte des Vereins. Ihm obliegt die Verwaltung des Vereinsvermögens, die Ausführung der Beschlüsse der Mitgliederversammlung sowie die Vorbereitung und Einberufung der Mitgliederversammlung. Der Vorstand ist Dienstvorgesetzter aller vom Verein angestellten Personen; er kann diese Aufgabe einem Vorstandsmitglied übertragen. Er ist zudem für alle Angelegenheiten des Vereins zuständig, die nicht durch die Satzung einem anderen Vereinsorgan zugewiesen sind.
+ Der Vorstand fasst seine Beschlüsse auf Vorstandssitzungen, die von einem Vorstand einberufen werden. Der Vorstand ist beschlussfähig, wenn mehr als die Hälfte der Vorstandsmitglieder anwesend sind. Die Tagesordnung braucht nicht zu angekündigt werden. Eine Einberufungsfrist von einer Woche ist grundsätzlich einzuhalten.
+ Der Vorstand fasst seine Beschlüsse mit einfacher Mehrheit der erschienenen Vorstandsmitglieder.
+ Der Vorstand ist verpflichtet, gerichtlich oder behördlich geforderte Satzungsänderungen durchzuführen und umzusetzen. Die Mitglieder sind hierüber umgehend zu informieren.
+ Ein vom Gesamtvorstand gewähltes Vorstandsmitglied fungiert gegebenenfalls als Repräsentant in anderen Gremien, Vereinen oder Institutionen denen der Verein angehört. Diese Aufgabe kann zeitlich begrenzt an ein anderes ordentliches Mitglied übertragen werden.
+ Die schriftführende Person erstellt und unterzeichnet ein Protokoll über die Beschlüsse des Vorstands. Die Schriftführung kann auch von einem anderen Vorstandsmitglied stellvertretend übernommen werden. Das Protokoll ist innerhalb einer Woche den Mitgliedern zur Verfügung zu stellen. Erfolgt nach der Veröffentlichung des Protokolls innerhalb von vier Wochen kein Einspruch gilt dieses als genehmigt.
+ Die kassenführende Person überwacht die Haushaltsführung und verwaltet unter Beachtung etwaiger Vorstandsbeschlüsse das Vermögen des Vereins. Sie hat auf eine sparsame und wirtschaftliche Haushaltsführung hinzuwirken. Mit dem Ablauf des Geschäftsjahres stellt sie unverzüglich die Abrechnung sowie die Vermögensübersicht und sonstige Unterlagen von wirtschaftlichem Belang den Finanzprüfenden des Vereins zur Prüfung zur Verfügung.

= Finanzprüfende
#set enum(numbering: n => "9." + [#n])
+ Zur Kontrolle der Haushaltsführung bestellt die Mitgliederversammlung Finanzprüfende. Nach Durchführung ihrer Prüfung setzen sie den Vorstand von ihrem Prüfungsergebnis in Kenntnis und erstatten der Mitgliederversammlung Bericht.
+ Die Finanzprüfenden dürfen dem Vorstand nicht angehören.

= Auflösung des Vereins, Vermögenbindung
#set enum(numbering: n => "10." + [#n])
+ Bei Auflösung oder Aufhebung des Vereins oder bei Wegfall steuerbegünstigter Zwecke fällt das Vermögen des Vereins an eine von der Mitgliederversammlung zu bestimmende juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte Körperschaft zwecks Verwendung für die Förderung der Bildung oder des Verbraucherschutzes im Sinne des § 2 dieser Satzung.
+ Wird dem Verein die Rechtsfähigkeit entzogen, so besteht er als nicht eingetragener Verein fort, so die Mitgliederversammlung nichts anderes beschließt.
+ Im Fall der Auflösung des Vereins erfolgt die Liquidation durch die zur Zeit der Auflösung amtierenden vertretungsberechtigten Vorstandsmitglieder zu den für die Beschlussfassung und Vertretung in der Satzung geregelten Bestimmungen, falls nicht die die Auflösung beschließende Mitgliederversammlung etwas anderes bestimmt.

= Inkrafttreten
#set enum(numbering: n => "11." + [#n])
+ Die Satzung tritt mit Gründung des Vereins in Kraft.
